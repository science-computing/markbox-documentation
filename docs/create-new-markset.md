# Create New MarkSet

---

A MarkSet is a bundled project: the combination of a test, a classlist, and all the processes required for marking the test, as well as all of the results of the test.

Previously created MarkSets can be viewed from the [MarkBox2.0 homepage](https://markbox2.uwaterloo.ca/). 

The three most recently modified MarkSets will be displayed at the top of the page with their respective MarkSet type and stage. 

To create a new MarkSet, click **Create New MarkSet** in the center of the page, the left navigation bar, or next to "All MarkSets". 

![New Markset](media/new-markset/newmarkset2.png) 

Next, type in the title of the MarkSet in the text field under "MarkSet Title:". The MarkSet title is used to self-reference this MarkSet. It is generally a good idea to include the current term and course number in the MarkSet Title. 

![MarkSet Title](media/new-markset/marksettitle.gif)

Next, select one of the four MarkSet types that will best suit the desired test: 

1. [**Classic Bubble Sheet**](marksets/classic-bubble-sheet.md)

- Standardized bubble sheet for multiple choice questions.

- Supports 180+ questions and up to 10 different versions per MarkSet.

![Completed Bubblesheet](media/complete-bubblesheet.png)

2. [**TestCreator/ Scrambled Multiple Choice Test**](marksets/scrambled-multiple-choice-test.md)

- By using the TestCreator tool, instructors can hand-craft a test which can support a variety of scrambling and feedback options.
   
- Currently, the old version of [MarkBox](https://www.markbox.uwaterloo.ca) is required to make **.test** files.

> PLEASE NOTE: The whole test must be generated through [MarkBox 1.0](https://www.markbox.uwaterloo.ca) with this option.

![Test Page Details](media/test-page-details.png)
![Scrambled Coversheet Filled](media/scramble-cover-filled.png)
![Sample Test Page](media/sample-page.png)

3. [**TA-marked Coversheet**](marksets/ta-marked-coversheet.md)

- Generates a coversheet intended for TAs / instructors to mark for fast and easy grade collection / returning to students.

![TA Marked Coversheet](media/ta-marked/ta-marked-filled.png)

4. [**Peer-marked Cover sheet**](marksets/peer-marked-coversheet.md)

- Intended for in-class exercises. 

- Provides space for both a student's and marker's information and and some workspace on the coversheet.

![Peer Marked Coversheet](media/peer-marked/peer-marked-filled.png)

---

After selecting a MarkSet, there will be a list of actions to complete to configure the test. Each MarkSet will have a specific list of actions to take in order to configure it.

Once a MarkSet is configured, the coversheet can be downloaded by clicking **Download Coversheet**. 

>PLEASE NOTE: A page size must be selected before being able to download a coversheet. 

After the tests are complete, click **Continue to Marking** to [mark the test](mark-test.md). 

>PLEASE NOTE: Once this is done, you cannot edit your MarkSet configurations. 

# Deleting a Markset

Open the MarkSet that needs to be deleted. 

Then, click **Delete Markset** on the left navigation bar. 

Deleting a MarkSet will permanently delete all data associated with it. This means all of the following will be unrecoverable:

- Uploaded Scans
- Processed Images
- Error Correction
- Final Grades
- Annotated PDFs

Please ensure that all necessary information has been downloaded and retained before continuing.

![Delete MarkSet](media/mark-your-test/delete-markset.gif)
