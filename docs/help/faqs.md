# Frequently Asked Questions 

---

### What does it mean when Markbox says I have Bad Tests/Scans? 

The cases where MarkBox will be unable to "read" the scans are: 
 
- the QR code is damaged 
- two or more of the triangles in the corner of the exam were damaged 

Unfortunately, the only way to currently resolve this is to manually mark these tests. Users can click **Download All** to download all of the tests or **Download** to download individual tests and then print them to mark them. 

By typing the Test Version number into the text field under **Manual Marking** to generate the answers to the respective version of the test. 

> PLEASE NOTE: These manually marked tests will not appear in the Test Stats. 
 
### Why don't my scans line up with the marking scheme? 

![Test Stats](../media/mark-your-test/correct-answers.gif)

Chances are you scanned in the wrong test type and mismatched your marksets. 

### What do I do if a scan is distorted? 

If the test scan is distorted in MarkBox and cannot be marked by the system, click **Remove from Automated Marking**. 

The test can then be manually marked from **Bad Tests/Scans**. 

Typing the Test Version number into the text field under **Manual Marking** will generate the answers to that test. 

### How to I make multiple answers correct? 

Sometimes students may be split between two or more arguably correct answers.

Although you may wish to make multiple "correct" answers to a question, this is simply not possible.  

If there is a bad question, you can edit the points a question is worth by clicking **Adjust Scoring**. To remove the question from marking, set the **Correct Points** to 0. 
