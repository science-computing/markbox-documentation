# Welcome to MarkBox 2.0

![MarkBox Logo](media/markboxlogo.png)

[MarkBox 2.0](https://markbox2.uwaterloo.ca) is the updated version of [MarkBox](https://markbox.uwaterloo.ca), with a superior user experience, faster processing times, and better integration with other university services. MarkBox is a home-grown OMR (Optical Mark Recognition) tool developed at the University of Waterloo. 

### Multiple MarkSets 

MarkBox now provides multiple customizable MarkSets to choose from for evaluations.

MarkSets are the updated version of Markbox 1.0's MarkBooks. 

To get started making your test, create a [new MarkSet](create-new-markset.md). 

### Academic Integrity 

Academic Integrity is a critical priority and is fundamental for testing, marking, and sending test results to students. 

Every student could receive a different test; the same questions and answers, but in a completely randomized order, customized to user preferences.

MarkBox 2.0 adds to its grading accuracy and enables students to see copies of their bubblesheets electronically, with clear indications of which answers MarkBox identified for marking. Students can clearly see which answers were correct and incorrect.

MarkBox is a complete improvement on the experience, usability, speed, and efficiency of using scantron cards. MarkBox immensely streamlines a previously manual process and makes it much easier to correct errors when marking tests. 

### Exciting Updates

•Faster PDF processing times with progress bars

•Removal of LaTeX processing, eliminating processing errors 

•Larger question limits

•Up to 10 versions in "Classic Bubble" mode

•Randomized multiple choice test maker/marker

• Return test electronically to students in a private, secure manner with feedback

• Records marks in a spreadsheet quickly

• Long answer, hand-marked test return and grade recording

• Hand marked tests can be done without intervention 

• Complete replacement for Scantron cards 

• Easy change to grading scheme 

• Personalized feedback to multiple choice tests

---

Log into MarkBox 2.0 to [get started](create-new-markset.md). 
