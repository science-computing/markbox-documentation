# Marking Your Test

---

After the test has been completed and collected, scan the completed pages to begin the marking process. Ideally, test scans should be compiled as a single PDF file for uploading. 

Only the bubblesheets need to be scanned for MarkBox to mark them, and doing so will significantly speed up the processing time. 

To scan full tests, ensure that they are in order to prevent pages from ending up as part of the wrong test.  

To mark the tests, complete the following Actions by clicking on them sequentially: 

- [Upload Scans](#1-upload-scans)
- [Correlate Scans](#2-correlate-scans)
- [Correct Errors](#3-correct-errors)
- [View Test Stats](#4-view-test-stats)
- [Download Gradesheet](#5-download-gradesheet)
- [Finalize Test](#6-finalize-test)

![Actions required for marking test](media/mark-your-test/marking-actions-2.png)

At any time during the marking process, it is possible to **Adjust Scoring** of the test questions if there were any bad questions, and **Re-Download Test** to download a copy of the test. 

To **View / Mark Bad Tests and Scans**, the scanned tests must first be uploaded and correlated. 

![Tools](media/mark-your-test/tools.png)

#### 1. Upload Scans 

To upload scanned tests, select whether **Only Bubble Sheets** or a **Full Scan** of the test will be uploaded. 

Then, click **Upload** in the bottom right corner of the hovering window. 

![Upload Test Scans](media/mark-your-test/upload-scans.png)

After uploading the test scans, MarkBox automatically processes the information on each coversheet and matches students from the student list to their test scan.

At this stage you have two options -- if you scanned your entire test select the **Full Test** option and Markbox will attempt to **group** each page into a test, associating each each page with is not a bubble sheet to the nearest bubble sheet. If you choose to go this route, please ensure that your test is scanned in the correct order.

It is generally recommended to *only* scan bubble sheets unless you intend on returning the full scanned test back to each student.


Once your PDF is uploaded, you will need to wait while the test processed.

#### 2. Correlate Scans

If a student incorrectly marked their student ID, or if a student who is not in the class submitted a test, they will appear in the "Un-correlated Students" table. 

![Uncorrelated Scans](media/mark-your-test/uncorrelated-students.png)

To match these students with their appropriate test and to disregard submissions from students not in the class, click **Correlate Scans**. 

An image of the hand-written student number will be displayed and can be typed into the field under "Student:" to find and correlate them. If there were multiple versions of the test, the student can also be matched to their respective test version.

![Correlating Students](media/mark-your-test/find-students.gif)

To remove a student not in the class, confirm that they aren't in the classlist by searching for their student ID, and then click **Student not in class list** in the bottom left. 

To remove a test submission from being marked, click **Remove test from automated marking** in the bottom right.

![Test Stats](media/mark-your-test/not-in-class.gif)

Once there are no more tests left to correlate, click **Done Correlating** in the bottom right corner of the page.  

>PLEASE NOTE: To continue to the next step, all tests must be correlated or removed from manual marking. Tests which appear to be badly distorted, or cannot be correctly correlated should be removed at this stage.

#### 3. Correct Errors

MarkBox identifies test selections with a blue rectangle around them. 

Sometimes, students make markings outside of the bubbles, mark multiple answers for the same question, leave answers blank, or fail to properly erase answers. MarkBox will process and highlight these errors which *must* be manually corrected to ensure that grades are distributed accurately. 

MarkBox will display a list of the test errors in the left navigation bar with the question number and type of error. Hovering over a question error will highlight the question and its respective test selections. 

Answers that have issues will also be marked with a red circle to the left of the question number on the test. 

If multiple selections are highlighted with the blue rectangle, click on selections that need to be removed. A circle will appear to the right of the question,indicating that the selection has been edited. 

If a student clearly made a selection and it was not recognized by MarkBox, click on the surrounding rectangle to highlight it and accept it as an answer. 

If there is no error in an answer but MarkBox highlights it as a problem answer, click **Ignore** under that specific question in the left navigation bar or click the red circle next to the question number which will become gray. If all of the questions are mis-marked as test errors, click **Ignore All** in the left navigation bar. 

To unignore a question, simply click **un-ignore** under that question in the left navigation bar or click on the circle next to the question number. 

![Fixing Errors](media/mark-your-test/fixing-errors.gif)

To assist in correcting test errors, use the **Manual Marking** to view the test answers. This is especially helpful for tests that have multiple versions. Simply enter the test version of interest into the text field to display the answers.  

![Manual Marking](media/mark-your-test/manual-marking.gif)

From here, any bad/mis-scans can be viewed in more detail. 

> PLEASE NOTE: Loose pages will not be recognized. They will be removed from automatic marking but can be manually marked later.

![Bad Tests](media/mark-your-test/bad-tests.png)

If a test needs to be removed from grading, click **Remove from Marking** at the bottom right of the page. 

![Remove Test from Marking](media/mark-your-test/remove-from-marking.png)

Once a test has had its errors corrected, click **Save and go to next test** to move on to correcting the next test. 


#### 4. View Test Stats

After ensuring that any grading errors have been corrected, the grade distribution for each version of the test can be examined.

The statistics shown will be: 

- **Answer Key Type** - The version of the test
- **Number of Tests** - Number of tests generated
- **Maximum Score** - Total number of available points
- **Average Score** - Average number of points scored 
- **Standard Deviation** - The extent of deviation for the class as a whole
- **Distribution of Grades (percent-based)** - The percentile grade distribution that students acheived 
- **Student Scores & Selections** - The individual selections that each student made in their test, presented as a downloadable **.csv** table. 
- **Question Analysis** - Each question, its value, correct answer, PtB, percentage of correct answers (%C), number of students who chose each possible answer, marked errors, and blank responses, presented as a downloadable **.csv** table.

>PLEASE NOTE: The maximum score will not necessarily be equal to the number of questions on the test. It will be equal to the sum of the weight of each question. If the weight of any of the questions was changed, this will affect the maximum score. 

![Test Stats](media/mark-your-test/test-stats-final.png)

>PLEASE NOTE: Only TestCreator tests will display the "Question Analysis" Table. 

#### 5. Download Gradesheet

Click **Download Gradesheet** to generate and download a **.csv** (spreadsheet) of all the students' results.

![Sample Gradesheet.csv file](media/mark-your-test/sample-gradesheet.png)

#### 6. Finalize Test

After everything has been marked and completed, click **Finalize Test**. 

By finalizing the test, the ability to do the following will be lost:

- Upload Scans
- Correlate Tests
- Alter Bubble Sheets (Correct Errors)

The following actions will remain available: 

- View Bad Scans
- Access Test Results
- Send Results to Students via Email
- Adjust scoring, with some limited functionality

>PLEASE NOTE: If changes are made here, all tests will need to be re-annotated and potentially re-emailed to students. "Emailed" status on all tests will be reset.

From the **Finished** page, the results of the tests can be emailed to each student. Students will only be able to see their own marked coversheet, their selections, and the MarkBox selections. 

![Email Students](media/mark-your-test/email.png)

If certain students need to be excluded from emails, potentially due to remarking specific tests, click the checkbox under "Exclude" next to the student's name. 

![Exclude students from email](media/mark-your-test/exclude-send.png)

