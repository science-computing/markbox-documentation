# MarkSet Modifications/Tools

---

The following tools may assist in correcting or validating issues with your test:

- [Re-download Test](#1-re-download-test)
- [View / Mark Bad Tests and Scans](#2-view-mark-bad-tests-and-scans)
- [Adjust Scoring](#3-adjust-scoring)

#### 1. Re-download Test

At any point in time, clicking **Re-Download Test** will download the original test to the default download location on the user's computer. 

#### 2. View / Mark Bad Tests and Scans

After [Uploading Tests] and [Correlating Scans], there may be rare instances where scans are failed to be detected by MarkBox and thus, cannot be marked by the System. 

Manually removed test scans from correlating scans will also appear here. 

To view and manually mark any bad tests and scans, click **View / Mark Bad Tests and Scans**. 

Manual Marking will display the answers to the test if there is only one version. If there are multiple versions, type the test version number into the text field under **Manual Marking** and click **Get Solutions**. All of the answers for that specific version will be displayed for ease in the manual marking process.

![Finding a test version for manual marking](media/test-version.gif)

The bad tests and scans will appear under **Bad Tests / Mis-Scans**. 

Click on a test to inspect it closer. 

Users can click **Download All** to download all of the tests or **Download** to download individual tests and then print them to mark them if desired.

> PLEASE NOTE: These bad tests cannot be re-integrated into your MarkSet but can be viewed and downloaded for archiving purposes here.

#### 3. Adjust Scoring

Sometimes, instructors may choose to change the correct answer, remove questions from marking, or award bonus points. This can be done by clicking **Adjust Scoring**. This will open the **Edit Test Answers** page.

Clicking **Download Unscrambled Test** will download the test in the order displayed. 

Clicking **Download Unscrambled Solutions** will download the solutions to each question in order, as currently selected. 

To change the correct answer for a question, click on the letter corresponding to the desired solution under a question. It will become highlighted. 

![Changing the correct answer](media/changing-answer.gif)

> PLEASE NOTE: You cannot have multiple correct answers. 

To add bonus marks for a question, increase the value of **Correct Points**.

To penalize an incorrect response, increase the value of **Incorrect Points**. This will add negative points to the overall score of the test if desired. 

To remove a question from marking, set the value of **Correct Points** and **Incorrect Points** to 0. 

To apply any changes made, you must click **Save Changes** in the bottom right corner. 
