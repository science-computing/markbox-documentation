# Classic Bubble Sheet

---

The Classic Bubble Sheet is a scannable coversheet that can be simply handed out or attached to the test. 

To configure the Classic Bubble Sheet, complete the following Actions by clicking on them sequentially: 

- [Select Page Size](#1-select-page-size)
- [Upload Student List](#2-upload-student-list)
- [Answer Key/Versions](#3-answer-key-and-test-versions)
- [Continue to Marking](#4-continue-to-marking)

![Bubblesheet Actions](../media/classic-bubblesheets/bubblesheet-actions.png)

#### 1. Select Page Size 

All questions must fit on one page, so choose the appropriate page size based on the number of questions (because the scanner picks up the marking triangles from the next page when double-sided).

- Letter (8.5x11inch) will fit a maximum of 180 questions

- Legal (8.5x14inch) will fit a maximum of 264 questions 

To *only* download the coversheet, click **Download Coversheet** after selecting a page size.

At this point, copies of the coversheet can be printed and distribute to classes before collecting and marking them. 

> PLEASE NOTE: Completed coversheets cannot be linked to students or to the correct answers without configuring the rest of the MarkSet.

![Page Size Selection](../media/classic-bubblesheets/page-size.gif)

#### 2. Upload Student List 

Upload a **.csv** of the classlist by clicking **Choose File**. 

If there are headings in the classlist table, click the checkbox next to "Skip the first row (remove headers)" near the bottom of the hovering window to remove the headers. 

>PLEASE NOTE: Clicking the checkbox next to "Skip the first row (remove headers)" if there are not table headers will remove the first student from the classlist. 

Click **Upload Students** to input the classlist into the MarkSet. 

![Student List](../media/classic-bubblesheets/student-list.png)

#### 3. Answer Key and Test Versions 

Classic Bubble Sheets can support up to 10 different versions of the test: A, B, C, D, E, F, G, H, I, and J.  

Answers can be typed into the text field or manually enter answers by clicking the correct option. 

![Inputting Answers](../media/classic-bubblesheets/typing-answers.gif)

If an incorrect answer is inputted, clicking on the correct choice or by typing the correct choice in place of the incorrect one in the text field will replace it. 

![Fixing Answers](../media/classic-bubblesheets/fixing-answers.gif)

To create multiple versions of the same test, click **Add Version**. 

To delete versions of the test, click **Delete Version**. 

>PLEASE NOTE: Test Versions cannot be modified, and must always be in ascending order. If there are versions A, B and C, and version B is deleted, version C will be relabelled as version B.

![Add and Delete Versions](../media/classic-bubblesheets/add-delete-versions.gif)

The value of each question can be adjusted at the bottom right by clicking **Edit Weighing**. 

To change the weight of a question, type in the desired value of that question within the text field next to it or by clicking the up/down arrows. 

Questions must have a minimum value of 0. If there is a bad question found after testing, the weight of test questions can be adjusted later during the marking process. 

![Weighing Answers](../media/classic-bubblesheets/weighting.png)

If satisfied with the Classic Bubble Sheet configurations, click **Save Versions**. 

![Save Versions](../media/classic-bubblesheets/save-versions.png)

Click **Download Coversheet** on the right of the MarkSet page to download a PDF file of the coversheet. 

Once completed coversheets are gathered from the class, the next step is to mark them. 

#### [4. Continue to Marking](../mark-test.md)
