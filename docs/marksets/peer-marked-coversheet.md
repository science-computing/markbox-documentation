# Peer-Marked Coversheet

---

The Peer-Marked Coversheet is the scannable coversheet intended for in-class peer-marking exercises.  

Space is provided for both the student and marker to bubble in their student ID or designated 8-character "code" and names. The gradesheet produced after marking will contain both users' information. 

To return results to the students via email, the marker's name and ID will be removed from the page.

To configure the Peer-Marked Coversheet, complete the following Actions by clicking on them sequentially: 

- [Configure Coversheet](#1-configure-coversheet)
- [Upload Student List](#2-upload-student-list)
- [Download Coversheet](#3-download-coversheet)
- [Continue to Marking](#4-continue-to-marking)

![Actions for Peer-Marked Coversheets](../media/peer-marked/actions.png)

#### 1. Configure Coversheet 

In order to fit the desired number of questions on the coversheet, choose the number of columns over which the test questions will be listed. 

- **One Column** will allow a maximum of 10 questions over one column, up to 15 points each. 
- **Two Columns** will allow a maximum of 20 questions over two columns, up to 5 points each  

Next, choose the paper size for the coversheet by clicking on the drop down menu next to "Paper Size". 

- Letter (8.5x11inch)
- Legal (8.5x14inch)

Next, within the text field next to "Number of Questions", enter a number at or below the question limit for the chosen column settings to generate questions for the coversheet. 

To set a default maximum score for all the questions, enter a value between 0-15 or 0-5 depending on the number of columns into the text field next to "Quick-set Max Scores" or by clicking the up/down arrows. 

![Quickset Scores](../media/peer-marked/peermarked-quickset.gif)

To add more questions manually,  click **Add Question** at the  bottom of the question list. 

Questions can be deleted by clicking the **X** on the far right next to the question under the "Delete" column. 

![Add & Delete Options](../media/peer-marked/add-delete.gif)

Once satisfied with the coversheet configuration, click **Save Config** in the bottom right corner to save the configuration. 

![Peer-Marked Coversheet Configurations](../media/peer-marked/configuration.png)

#### 2. Upload Student List 

For Peer-Marked Coversheets, it is recommended to add a "unknown" student to the classlist to handle cases where the marker fails to input their information, or a student not in the classlist acts as a marker.

Upload a **.csv** of the classlist by clicking **Choose File**. 

If there are headings in the classlist table, click the checkbox next to "Skip the first row (remove headers)" near the bottom of the hovering window to remove the headers. 

>PLEASE NOTE: Clicking the checkbox next to "Skip the first row (remove headers)" if there are not table headers will remove the first student from the classlist. 

Click **Upload Students** to input the classlist into the MarkSet. 

![Student List](../media/classic-bubblesheets/student-list.png)

#### 3. Download Coversheet 

Click **Download Coversheet** from the "Actions" list to download a PDF file of your coversheet. 

At this point, copies of the coversheet can be printed and distribute to classes before collecting and marking them. 

#### [4. Continue to Marking](../mark-test.md)

