# TestCreator/ Scrambled Multiple Choice Test

---

The TestCreator tool allows instructions to create a hand-crafted test which supports a variety of scrambling and feedback options. 

To create a Scrambled Multiple Choice Test, the TestCreator tool must be used to generate a **.test** file from [MarkBox 1.0](https://markbox.uwaterloo.ca). 

To configure the Scrambled Multiple Choice Test, complete the following Actions by clicking on them sequentially: 

- [Upload Student List](#1-upload-student-list)
- [Configure Question and Answers](#2-configure-questions-and-answers)
- [Printing Options](#3-printing-options)
- [Generate Sheets](#4-generate-sheets)
- [Download Test](#5-download-test)
- [Continue to Marking](#6-continue-to-marking)

![Scrambled Test Actions](../media/test-creator/testcreator-options.png)

#### 1. Upload student list 

Upload a **.csv** of the classlist by clicking **Choose File**. 

If there are headings in the classlist table, click the checkbox next to "Skip the first row (remove headers)" near the bottom of the hovering window to remove the headers. 

>PLEASE NOTE: Clicking the checkbox next to "Skip the first row (remove headers)" if there are not table headers will remove the first student from the classlist. 

Click **Upload Students** to input the classlist into the MarkSet. 

![Student List](../media/classic-bubblesheets/student-list.png)

#### 2. Configure Questions and Answers 

To input the questions and answers for the test, upload the **.test** file created in the TestCreator. 

![Upload from MarkBox 1.0](../media/test-creator/import-test.gif)

To learn how to use the MarkBox 1.0 TestCreator, please review the [MarkBox 1.0 User Guide](https://uwaterloo.ca/science-computing/markbox1-user-guide) to become acquainted with it. 

![MarkBox 1.0](../media/test-creator/markbox1.png)

>PLEASE NOTE: If there are issues accessing the old version of MarkBox, please contact [Mirko Vucicevich](mailto:mvucicevich@uwaterloo.ca).

Once uploaded, the current Test Configuration (number of pages, number of question groups, number of questions, and page size) and Test Preview will be presented. 

![Current TestCreator Configuration](../media/test-creator/test-page-details.png)

A PDF with *only one* copy of the test can be downloaded by clicking **Download PDF** to the right of "Test Preview". 

Click **Confirm** in the bottom right corner of the page once satisfied to complete the remaining configuration actions. 

#### 3. Printing Options 

The following options will set or alter the document generation process for this MarkSet:

**Download type**: Select the desired output file type; either a single combined PDF of all the tests or a Zip File containing separate test files. 

**Bubble sheet location**: Select whether to print the bubble sheets at the FRONT or BACK of each test.

**Double sided**: Select whether or not to print single or double-sided pages. Markbox will insert blank pages when needed to ensure that the bubble sheet is detachable.

**Number of Tests**: Enter the number of tests to generate in the test field. 0 will generate a number of tests equal to the class size.

Click **Save Options** in the bottom right of the hovering window to confirm the selections and apply them to the test configuration. 

![Printing Options Filled](../media/test-creator/printing-options.png)

#### 4. Generate Sheets 

Once satisfied with the test configuration, clicking **Generate Sheets** from the Action list will download the printable test file based on the **Download type** setting.  

Depending on the size of the test, it may take a few minutes to produce the download. MarkBox will display a progress bar as it processes the test pages to help gauge how long the test generation will take. 

The test will generate as a background process, so it is okay to close the browser. 

![Processing Sheets](../media/test-creator/processing.png)]

#### 5. Download Test

Once downloaded, print the test file and distribute the tests to the class before collecting and marking them. 

#### [6. Continue to Marking](../mark-test.md)

