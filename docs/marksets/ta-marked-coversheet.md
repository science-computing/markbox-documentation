# TA-Marked Coversheet

---

The TA-Marked Coversheet is the scannable coversheet that is intended for use-cases where you'd like graders to fill out the bubbles.

A good example would be a series of short-answer questions where each question is worth a certain amount of points. Simply generate your cover sheets and attach them to the front of the test/quiz and have the students only fill out their name and student ID.

Your markers can then record the final grades for each question on the coversheet. MarkBox allows you to scan the entire test instead of just the coversheet, which can allow for very fast collection of grades, as well as a convenient way to return the hand-marked document back to your students.

To configure the TA-Marked Coversheet, complete the following Actions by clicking on them sequentially: 

- [Configure Coversheet](#1-configure-coversheet)
- [Upload Student List](#2-upload-student-list)
- [Download Coversheet](#3-download-coversheet)
- [Continue to Marking](#4-continue-to-marking)

![Actions for TA Marked Coversheets](../media/peer-marked/actions.png) 

#### 1. Configure Coversheet 

In order to fit the desired number of questions on the coversheet, choose the number of columns over which the test questions will be listed. 

- **Two Columns** will allow a maximum of 60 questions over two columns, up to 15 points each. 
- **Three Columns** will allow a maximum of 90 questions over three columns, up to 10 points each  

Next, choose the paper size for the coversheet by clicking on the drop down menu next to "Paper Size". 

- Letter (8.5x11inch)
- Legal (8.5x14inch)

Next, within the text field next to "Number of Questions", enter a number at or below the question limit for the chosen column settings to generate questions for the coversheet.  

To set a default maximum score for all the questions, enter a value between 0-15 or 0-10 depending on the number of columns into the text field next to "Quick-set Max Scores" or by clicking the up/down arrows. 

![Quickset Scores](../media/ta-marked/ta-quickset.gif)

To add more questions manually, click **Add Question** at the  bottom of the question list. 

Questions can be deleted by clicking the **X** on the far right next to the question under the "Delete" column. 

![Add & Delete Options](../media/peer-marked/add-delete.gif)

Once satisfied with the coversheet configuration, click **Save Config** in the bottom right corner to save the configuration. 

![TA Coversheet Configurations](../media/ta-marked/ta-configuration.png)

#### 2. Upload Student List 

Upload a **.csv** of the classlist by clicking **Choose File**. 

If there are headings in the classlist table, click the checkbox next to "Skip the first row (remove headers)" near the bottom of the hovering window to remove the headers. 

>PLEASE NOTE: Clicking the checkbox next to "Skip the first row (remove headers)" if there are not table headers will remove the first student from the classlist. 

Click **Upload Students** to input the classlist into the MarkSet. 

![Student List](../media/classic-bubblesheets/student-list.png)

#### 3. Download Coversheet 

Click **Download Coversheet** from the "Actions" list to download a PDF file of the coversheet. 

At this point, copies of the coversheet can be printed and distribute to classes before collecting and marking them.  

#### [4. Continue to Marking](../mark-test.md)
